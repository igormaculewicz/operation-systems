#!/bin/bash

read -p 'Specify folder name:' folder_name
read -p 'Specify files amount: ' files_amount

echo "Creating $folder_name folder with $files_amount files in it."

mkdir $folder_name
cd $folder_name

for i in $(seq 1 ${files_amount})
do
   echo "This is file number: $i" >> "$i.txt"
done