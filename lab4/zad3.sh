#!/bin/bash

countEntriesInPathByType(){
find $1 -mindepth 1 -maxdepth $2 -type $3 -readable 2>&1 | wc -l
}

read -p 'Specify path:' path
read -p 'Specify depth: ' depth

echo "Searching ${path} for ${depth} layers down."

directories=$(countEntriesInPathByType ${path} ${depth} d)
files=$(countEntriesInPathByType ${path} ${depth} f)

echo "Found:"
echo "Directories; ${directories}"
echo "Files: ${files}"