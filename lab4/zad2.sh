#!/bin/bash

read -p 'Specify file name:' file_name
read -p 'Specify sign: ' sign

#First part obtaining given sign by -o flag(It matches only given substring), then counting a lines.
result=$(grep -o "${sign}" ${file_name} | wc -l)

echo "Character '${sign}' in file '${file_name}' occurs ${result} times."