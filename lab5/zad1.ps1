$folderName = Read-Host -Prompt 'Input folder name'
$filesAmount = Read-Host -Prompt 'Input files amount'

$dir = New-Item -Name $folderName -ItemType "directory"

Write-Output "Creating $folderName folder with $filesAmount files in it."

for($i = 1; $i -le $filesAmount; $i++)
{
    $dir = New-Item -Name $folderName/$i.txt -ItemType "file"
    "This is file number: $i" > $dir
}