$path = Read-Host -Prompt 'Input path'
$depth = Read-Host -Prompt 'Input depth'

Write-Output "Searching $path for $depth layers down."

$directories = (Get-ChildItem -Directory -Recurse -Depth $depth -ErrorAction SilentlyContinue $path | Measure-Object).Count
$files = (Get-ChildItem -File -Recurse -Depth $depth -ErrorAction SilentlyContinue $path | Measure-Object).Count

Write-Output "Found:"
Write-Output "Directories: $directories"
Write-Output "Files: $files"