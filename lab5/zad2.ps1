$fileName = Read-Host -Prompt 'Input file name'
$sign = Read-Host -Prompt 'Input sign'

$result = (Select-String -Path $fileName -Pattern $sign -AllMatches).Matches.Count

Write-Output "Character '$sign' in file '$fileName' occurs $result times."