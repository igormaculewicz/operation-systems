# Character counter
###### by Igor Maculewicz

## TL;DR;
This application realizes a functionality of couting a given letter in given file, and then save output to given file. 

## Project structure
├── file_operations  
│   &ensp;&ensp;&ensp; ├── file_operations.c  
│   &ensp;&ensp;&ensp; └── file_operations.h  
├── functions  
│  &ensp;&ensp;&ensp;  ├── functions.c  
│  &ensp;&ensp;&ensp;  └── functions.h  
├── main.c  
├── Makefile  
└── resources  
    └── input.txt  

- **file_operations** – Contains file operations methods. 
- **functions** – Contains all convenient functions. 
- **main.c** – File which contains a main application method. 
- **Makefile** – Makefile which realizes lifecycle of application assembly. You can also perform run goal to run up assembled application with example data. 
- **resources** – Contains all resources necessary to make application work. 
- **input.txt** – Example input file. 

## How to run
Simply run
```sh
make
```
in main directory to build distribution package and run sample.d