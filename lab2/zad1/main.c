#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <memory.h>

int countCharacterInLine(char *line, const char character) {
    int counter;
    for (counter = 0; line[counter]; line[counter] == character ? counter++ : *line++);
    return counter;
}

void countCharactersLineByLine(char *filename, const char character, void (*counterFunction)(char *, const char)) {

    FILE *stream;
    char *line = NULL;
    size_t len = 0;

    stream = fopen(filename, "r");
    if (stream == NULL) {
        perror(filename);
        exit(EXIT_FAILURE);
    }

    printf("[Parent PID: %d] Counting in file named '%s' given char: '%c'\n", getpid(), filename, character);
    while (getline(&line, &len, stream) != -1) {
        line[strlen(line) - 1] = '\0';

        if (fork() == 0) {
            printf("[Parent PID, %d, Children PID: %d] ", getppid(), getpid());
            (*counterFunction)(line, character);
            break;
        }
    }

    free(line);
    fclose(stream);
    exit(EXIT_SUCCESS);
}

void countCharacter(char *line, const char character) {

    int charCounter = countCharacterInLine(line, character);

    printf("Count is '%d' for line: '%s' \n", charCounter, line);
}

int main() {
    countCharactersLineByLine("resources/input.txt", ' ', countCharacter);
}