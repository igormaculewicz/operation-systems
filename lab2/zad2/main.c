#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <memory.h>
#include <stdbool.h>
#include <dirent.h>
#include <time.h>

char *formatLogFilename(char *date) {
    const char *format = "log-%s.txt";

    size_t needed = snprintf(NULL, 0, format, date) + 1;
    char *buffer = malloc(needed);
    sprintf(buffer, format, date);
    return buffer;
}

char *getTime(const char *__format) {
    size_t FORMATTED_TIME_BUFFER_SIZE = 100;
    time_t timer;
    char *buffer = malloc(FORMATTED_TIME_BUFFER_SIZE);
    struct tm *timeInfo;

    timer = time(NULL);
    timeInfo = localtime(&timer);

    size_t usedSize = strftime(buffer, FORMATTED_TIME_BUFFER_SIZE, __format, timeInfo);
    buffer = realloc(buffer, usedSize);
    return buffer;
}

void sleepMilis(long milis) {
    usleep((__useconds_t) (milis * 1000));
}

int countDirectoryEntries(DIR *directory) {
    int fileCount = 0;
    struct dirent *entry;

    while ((entry = readdir(directory)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        fileCount++;
    }

    rewinddir(directory);
    return fileCount;
}

void scheduleFileCounting(char *directory, int milis, void(*operationFunction)(char *, int)) {
    DIR *dirp = opendir(directory);

    if (dirp == NULL) {
        perror(directory);
        exit(EXIT_FAILURE);
    }

    if (milis <= 0) {
        fprintf(stderr, "Wrong amount of milis: %d", milis);
        exit(EXIT_FAILURE);
    }

    while (true) {
        int counterEntries = countDirectoryEntries(dirp);
        operationFunction(directory, counterEntries);

        sleepMilis(milis);
    }
}

void handleEntries(char *directory, int countedEntries) {

    char *date = getTime("%Y-%m-%d");
    char *filename = formatLogFilename(date);

    FILE *file = fopen(filename, "a+");

    if (file == NULL) {
        perror(filename);
        exit(EXIT_FAILURE);
    }

    char *format = "[%s] There is '%d' in '%s' directory.\n";
    char *time = getTime("%Y-%m-%d %H:%M:%S");

    fprintf(file, format, time, countedEntries, directory);
    printf(format, time, countedEntries, directory);

    fclose(file);
}

int main(int argc, char *argv[]) {
    char *directory = argv[1];
    int milis = atoi(argv[2]);

    scheduleFileCounting(directory, milis, handleEntries);
}