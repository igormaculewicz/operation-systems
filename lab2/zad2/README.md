## How to run
Simply run
```sh
make
```
in main folder to run example.

#### OR
Unzip zad2.zip and run given command
```sh
./binary.bin <path_of_folder> <miliseconds>
```