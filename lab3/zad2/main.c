#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <memory.h>

void sigint_handler() {
    char decision;
    printf("Do you want to end?(y/n): ");
    scanf(" %c", &decision);

    if (decision == 'y') {
        exit(EXIT_SUCCESS);
    }
}

void switchValues(int *left, int *right) {
    int tmp = *left;
    *left = *right;
    *right = tmp;
}

int main(int argc, char *argv[]) {

    if (argv[1] == NULL || argv[2] == NULL) {
        fprintf(stderr, "Specify number range in first two arguments");
        exit(EXIT_FAILURE);
    }

    int from = atoi(argv[1]);
    int to = atoi(argv[2]);

    if (from > to) {
        switchValues(&from, &to);
    }

    int iterator = from;

    signal(SIGINT, sigint_handler);

    while (1) {

        printf("%d\n", iterator);
        sleep(1);

        if (iterator < to) {
            iterator++;
        } else {
            iterator = from;
        }
    }
}