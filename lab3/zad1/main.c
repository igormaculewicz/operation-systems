#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>

int *createForks(int forksAmount) {


    int *forkList = malloc(forksAmount * sizeof(int));

    for (int i = 0; i < forksAmount; i++) {
        forkList[i] = fork();
        if (forkList[i] == 0) {
            for (;;) {}
        }
    }

    return forkList;
}


void endIfCan(int *forkList, int forkAmount) {

    int livingProcesses = 0;

    for (int i = 0; i < forkAmount; i++) {
        if (forkList[i] >= 0) {
            livingProcesses++;
        }
    }

    if (!livingProcesses) {
        printf("There is no active child processes left. Exiting...");
        exit(EXIT_SUCCESS);
    }
}

void printForkList(int *forkList, int forkAmount) {

    for (int i = 0; i < forkAmount; i++) {
        if (forkList[i] >= 0) {
            printf("%d. Fork [ParentPID: %d, ChildrenPID: %d]\n", i, getppid(), forkList[i]);
        } else {
            printf("%d. Fork is already dead.\n", i);
        }
    }

}

int main(int argc, char *argv[]) {

    if (argv[1] == NULL) {
        fprintf(stderr, "Specify a amount of forks in first argument!");
        exit(EXIT_FAILURE);
    }

    int amount = atoi(argv[1]);

    int *forkList = createForks(amount);

    int decision;

    while (1) {
        printForkList(forkList, amount);

        printf("Which child process do you want to kill(Enter index): ");
        scanf("%d", &decision);

        if (decision >= 0 && decision < amount) {
            int result = kill(forkList[decision], SIGKILL);
            if (result >= 0) {
                printf("Killed process with pid: '%d', by signal: %d\n", forkList[decision], SIGKILL);
                forkList[decision] = SIGKILL * -1;
            } else {
                perror("Cannot kill child process: ");
                printf("\n");
            }
        } else {
            fprintf(stderr, "Wrong index!\n");
        }

        endIfCan(forkList, amount);
    }
}